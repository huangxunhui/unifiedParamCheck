package com.hxh.unified.param.check.controller;

import com.hxh.unified.param.check.example.doc.v1.V1DocForm;
import com.hxh.unified.param.check.example.doc.v2.V2DocForm;
import com.hxh.unified.param.check.form.*;
import com.hxh.unified.param.check.utils.ResultVoUtil;
import com.hxh.unified.param.check.validator.group.Insert;
import com.hxh.unified.param.check.validator.group.Update;
import com.hxh.unified.param.check.vo.ResultVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.regex.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2020/3/4 6:18 下午
 * Utils: Intellij Idea
 * Description:
 */
@RestController
public class TestController {

    /**
     * 表单请求
     * @param form 请求参数
     * @return 响应数据
     */
    @GetMapping("/formRequest")
    public ResultVo formRequest(@Validated RequestForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * JSON请求
     * @param form 请求参数
     * @return 响应数据
     */
    @PostMapping("/jsonRequest")
    public ResultVo jsonRequest(@RequestBody @Validated RequestForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * 案例测试
     * @param form 请求参数
     * @return 响应数据
     */
    @PostMapping("/exampleTest")
    public ResultVo jsonRequest(@RequestBody @Validated ExampleForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * 获取电话号码信息
     */
    @GetMapping("/phoneInfo/{phone:^[1][3,4,5,7,8][0-9]{9}$}")
    public ResultVo phoneInfo(@PathVariable("phone") String phone){
        return ResultVoUtil.success(phone);
    }

    /**
     * 获取电话号码信息
     */
    @GetMapping("/phoneInfo/{phone}")
    public ResultVo telInfo(@PathVariable("phone") String phone){
        // 验证电话号码是否有效
        String pattern = "^[1][3,4,5,7,8][0-9]{9}$";
        boolean isValid =  Pattern.matches(pattern, phone);
        if(isValid){
            // 执行相应逻辑
            return ResultVoUtil.success(phone);
        } else {
            // 返回错误信息
            return ResultVoUtil.error("手机号码无效");
        }
    }


    @PostMapping("/customTest")
    public ResultVo customTest(@RequestBody @Validated CustomForm form){
        return ResultVoUtil.success(form.getPhone());
    }

    /**
     * 添加用户
     */
    @PostMapping("/addUser")
    public ResultVo addUser(@RequestBody @Validated({Insert.class}) UserForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * 更新用户
     */
    @PostMapping("/updateUser")
    public ResultVo updateUser(@RequestBody @Validated({Update.class}) UserForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * 编辑用户
     */
    @PostMapping("/editUser")
    public ResultVo editUser(@RequestBody @Validated UserForm form){
        return ResultVoUtil.success(form);
    }

    /**
     * 自定义分组
     */
    @PostMapping("/customGroup")
    public ResultVo customGroup(@RequestBody @Validated CustomGroupForm form){
        return ResultVoUtil.success(form);
    }


    @PostMapping("/v1/docForm")
    public ResultVo v1DocForm(@RequestBody @Validated V1DocForm form){
        return ResultVoUtil.success(form);
    }

    @PostMapping("/v2/docForm")
    public ResultVo v2DocForm(@RequestBody @Validated V2DocForm form){
        return ResultVoUtil.success(form);
    }

}
