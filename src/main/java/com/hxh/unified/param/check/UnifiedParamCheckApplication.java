package com.hxh.unified.param.check;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnifiedParamCheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnifiedParamCheckApplication.class, args);
    }

}
