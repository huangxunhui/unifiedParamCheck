package com.hxh.unified.param.check.form;

import com.hxh.unified.param.check.validator.CustomSequenceProvider;
import lombok.Data;
import org.hibernate.validator.group.GroupSequenceProvider;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/21 6:18 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
@GroupSequenceProvider(value = CustomSequenceProvider.class)
public class CustomGroupForm {

    /**
     * 类型
     */
    @Pattern(regexp = "[A|B]" , message = "类型不必须为 A|B")
    private String type;

    /**
     * 参数A
     */
    @NotEmpty(message = "参数A不能为空" , groups = {WhenTypeIsA.class})
    private String paramA;

    /**
     * 参数B
     */
    @NotEmpty(message = "参数B不能为空", groups = {WhenTypeIsB.class})
    private String paramB;

    /**
     * 分组A
     */
    public interface WhenTypeIsA {

    }

    /**
     * 分组B
     */
    public interface WhenTypeIsB {

    }

}
