package com.hxh.unified.param.check.example.doc.v2;

import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/22 2:26 下午
 * Utils: Intellij Idea
 * Description:
 */
public class DocSequenceProvider implements DefaultGroupSequenceProvider<V2DocForm> {

    @Override
    public List<Class<?>> getValidationGroups(V2DocForm form) {
        List<Class<?>> defaultGroupSequence = new ArrayList<>();

        defaultGroupSequence.add(V2DocForm.class);

        if (form != null && "phone".equals(form.getDocType())) {
            defaultGroupSequence.add(V2DocForm.phone.class);
        }

        if (form != null && "idCard".equals(form.getDocType())) {
            defaultGroupSequence.add(V2DocForm.idCard.class);
        }

        if (form != null && "bankCard".equals(form.getDocType())) {
            defaultGroupSequence.add(V2DocForm.bankCard.class);
        }

        return defaultGroupSequence;
    }
}
