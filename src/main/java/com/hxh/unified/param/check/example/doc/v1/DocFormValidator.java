package com.hxh.unified.param.check.example.doc.v1;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/23 3:41 下午
 * Utils: Intellij Idea
 * Description:
 */
public class DocFormValidator implements ConstraintValidator<DocFormAnnotation, V1DocForm> {

    @Override
    public boolean isValid(V1DocForm value, ConstraintValidatorContext context) {
        String docType = value.getDocType();
        String docContent = value.getDocContent();

        if(docType == null || docContent == null){
            return false;
        }

        String pattern;

        // 可以根据类型从数据库获取

        switch (docType){
            case "phone":
                pattern = "^[1][3,4,5,7,8][0-9]{9}$";
                break;
            case "idCard":
                pattern = "^\\d{15}|\\d{18}$";
                break;
            case "bankCard":
                pattern = "^[1-9]\\d{12,18}$";
                break;
            default:
                pattern = "";
        }

        return Pattern.matches(pattern, docContent);

    }
}
