package com.hxh.unified.param.check.form;

import com.hxh.unified.param.check.validator.annotion.Phone;
import lombok.Data;

/**
 * @author huangxunhui
 * Date: Created in 2020/9/21 6:18 下午
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class CustomForm {

    /**
     * 电话号码
     */
    @Phone
    private String phone;

}
