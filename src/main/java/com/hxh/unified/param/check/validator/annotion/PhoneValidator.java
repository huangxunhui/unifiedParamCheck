package com.hxh.unified.param.check.validator.annotion;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * @author cyx
 * @date 2020/8/24 17:29
 * Utils: Intellij Idea
 * Description: 手机号码格式校验
 */
public class PhoneValidator implements ConstraintValidator<Phone, String> {

    @Override
    public boolean isValid(String telephone, ConstraintValidatorContext constraintValidatorContext) {
        String pattern = "^[1][3,4,5,7,8][0-9]{9}$";
        return Pattern.matches(pattern, telephone);
    }
}
